# Git Workflow

Learn here about the typical workflow we use for LibreCube projects.

## Set up your local repository

> Note: If the project you want to work on does **not** exist yet (ie. a new prototype),
> please first inform the admin to create an empty project (from template).

Follow these steps to clone an existing project in order to work on it.

1. Clone the LibreCube repository to your computer: `git clone <repo>`.
1. Change directory into the repository `cd <repo>` .
1. Create a new branch: `git branch <my_branch_name>`.
1. Make that branch active: `git checkout <my_branch_name>`
1. Do your work. Commit the changes: `git commit -m <my-commit-message>`. Do this frequently.
1. Also, frequently push your work to the remote repository: `git push`.
1. When all ready, inform the project admin(s) to review your work. You can use a merge request for that.

## Branching

The `main` branch contains the latest version of the project and is tested and runnable.

Urgent fixes may be applied directly to `main`.

The `develop` branch is taken from `main` and typically a bit ahead of `main`.
It is also tested and runnable and will be merged into `main` at some point.

Think of the `develop` branch as being used for sprints. At the begin of a sprint,
feature and fixes branches are based off from `develop`. Eventually, they will
be merged into `develop` when completed.

Typically you would name your branch like `issue-x`, where x is the number of the relevant
issue that was raised in the GitLab repository.

![](assets/git_branches.png)

## Commits

Write your commit messages in present tense and start with the commit type.
Add a semicolon ";" if there are more than one sentence.

- `[add]`: The new feature you're adding to a particular application
- `[mod]`: A modification to the code that changes functionality
- `[fix]`: A bug fix or similar
- `[refactor]`: Code improvements that do remain same functionality
- `[test]`: Everything related to testing
- `[docs]`: Everything related to documentation
- `[minor]`: For really minimal changes

Example:

```
[fix] Wrong type conversion of sender's .emit() method
[add] Parameter `dist` to Entity class constructor
[test] Simulate loss of frames during UDP transport; Extend unit test cases
```

For minor changes, a simple comment `minor` is sufficient.

## Rebasing

When working on your issue branch the develop branch may evolve due to other features and fixes being merged into develop. To take those changes into your branch use git rebasing. You may apply this every time before starting working on your branch. The typical workflow is as follows:

- Checkout develop:

```
$ git checkout develop
```

- Make sure develop is up to date:

```
$ git pull
```

- Checkout your branch:

```
$ git checkout issue-01
```

- Rebase with develop:

```
$ git rebase develop
```

- Alternatively, run the rebasing in an interactive way:

```
$ git rebase -i develop
```

- If merge issues are encountered (because conflicting changes were done in same files) you have to resolve them. Most IDEs provide a merge tools for this. Or use the command line:

```
$ git mergetool
```

- After fixing merge issues (if any) continue with rebase:

```
$ git rebase --continue
```

- If you make a mistake while merging, abert the merge to discard all changes you've made:

```
$ git rebase --abort
```

- Finally push changes to the remote repository. You need to force the push, since the commit history of the remote feature branch will be altered:

```
$ git push -f
```

## Merging

After completion of your issue branch, raise a merge request to have it
merged into `develop`, after being reviewed by one of the project owners.
