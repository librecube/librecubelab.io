# Development

LibreCube projects are community-maintained and we appreciate contributions from anyone.
Everyone is welcome to join the LibreCube community, there is no formalism attached to it.
Simply start contributing!

In order to ensure efficient and effective contributions that are of good quality, we have put in place some rules and guidelines. Please make sure to read them before making your first contribution.

## Git Repositories

All our hardware and software projects are hosted on [GitLab](https://gitlab.com/librecube). They are categorized as following:

- [Prototypes](https://gitlab.com/librecube/prototypes): New developments usually start out as prototypes. They are under more or less heavy development until they reach a point where they are then moved into one of the following categories.
- [Tools](https://gitlab.com/librecube/templates_and_tools): Various stuff to help you during development.
- [Library](https://gitlab.com/librecube/lib): In this category we have software modules that serve specific purposes. Many of them are written in Python. They can be integrated into other projects (for example, by the projects in the Elements category) to provide their functionality. A lot of communication protocol implementations are found here.
- [Elements](https://gitlab.com/librecube/elements): Here is the core of what we are doing. This are the hardware and software repositories that are the components of the LibreCube ecosystem. Everything in this category is stable and ready to be used in production. Expect very infrequent changes here, apart from occasional updates and bug fixes.

## Contribution Guidelines

Read the [Coding Style](coding_style.md) guideline to learn how to write beautiful and coherent code. This is crucial, because your code will be read by a lot of people, including your future self 😄.

Typically, there is more than one person to contribute code to a project. Thus, we need to have a proper workflow to ensure that this works smoothly. Please read the [Git Workflow](./git_workflow.md) guidelines on how to do that.

## Work Organization

LibreCube contributors come from various parts of the world and from all walks of live. Different time zones and availability makes it a challenge to coordinate the overall work. Therefore, lots of organization is coordinated offline.

You can raise specific issues and merge requests on the individual git repositories.

If you are looking for specific tasks to work on, have a look at our [issue board](https://gitlab.com/librecube/organization/-/boards).

The best way however is to first use our [Chat](https://app.element.io/#/room/#librecube.org:matrix.org) where you can start/join a discussion!
