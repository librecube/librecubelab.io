# Projects

At LibreCube we are developing an
[ecosystem of modular components](https://gitlab.com/librecube/elements) that,
when put together, result in exciting exploration systems!

We are working on such system developments as well, for three main reasons:

1. We want to see our components being used in such systems, to demonstrate
   the power of having an open source ecosystem.

2. When building such exploration systems, it drives the needs for developing
   or improving components that then become part osf the ecosystem.

3. It's much fun!

## Projects in Development

- [LibreCubeRover](rover/index.md): Development of space-grade rover of various sizes and capabilities.
- [LibreSim](simulator.md): An ECSS-SMP2 compliant spacecraft simulator
