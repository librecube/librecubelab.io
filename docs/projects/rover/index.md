# LibreCubeRover Project

## Objectives

The objective of this project is to develop a rover platform that will be
a practical testbed for existing elements and those under development of the
[LibreCube ecosystem](https://gitlab.com/librecube/elements).

As the focus of LibreCube is on space technology, we will interface with the
rover using space communications protocols and design it according to space
engineering concepts. Think of it not as a rover, but as a "satellite with wheels".

We also have a strong focus on automation, and this project provides great
opportunities for automation and also machine learning (for example, autonomous driving).

The minimum goals we want to achieve with the rover are:

- Rover shall be controlled via remotely sent commands
- Rover shall send telemetry (ie, status information) to remote
- Files shall be transferred to/from the rover
- Rover shall be able to do basic movements (drive, turn)

For the control, monitoring, and file exchange,
[CCSDS and ECSS protocols](../../reference_architecture/overview.md#protocols)
shall be used.

The extended goals are:

- Capture (or possibly stream) images with camera
- Implement onboard software with CCSDS/ECSS stack
- Implement automation (remote and onboard) based on ECSS PLUTO language

Further extensions will be added over time, such as laser pointer, lidar sensor,
sound module, etc.

![](assets/rover_image.jpg)

## Architecture

### Overview

The rover shall be controlled remotely. From high level perspective there is
a ground segment and the remote segment (the rover). The ground segment will
be kept as simple as possible, and be extended later. The development focus
for now is mostly on the rover.

![](assets/rover_scenario.png)

### Components

The basic system components of the rover are:

- **Structure**: It provides housing for the electronic board stack and fixtures for the wheels.
- **Power Module**: It supplies the rover with electrical power from batteries. Can be re-charged.
- **Communications Module**: For wireless data exchange with the rover.
- **Processing Module**: The onboard computer manages the rover and is the central interface.
- **Wheel Drive Module**: Controls the movement of the wheels

The additional system components are:

- **Camera Module**: To capture images, possible stream video.
- **Solar Panels**: Provide charging power for the rover.
- **Battery Expansion Module**: Extend the battery capacity of the rover.
- **Navigation Module**: Provide position information and heading.
- _further components to be defined_

### Interfaces

The boards follow the [LibreCube board specification](../../standards/board_specification/index.md).
The specification defines the mechanical shape of the board and the pin definition
of the 104-pin connector.

The onboard data exchange between processing module and other subsystems takes
place over three busses:

- **System Bus**: The processing module controls and monitors the other boards. It uses the redundant CAN bus and follows the [SpaceCAN protocol](../../standards/spacecan/index.md/). On this bus the processing module acts as controller and sends commands to other subsystems (responders). It also receives housekeeping data from the other subsystems. All subsystems are connected to the system bus, except the power system, which (currently) does not implement a responder node.

- **Comms Bus**: The comms bus interconnects the communications module and the processing module. It uses a redundant UART and transfers CCSDS frames between the two. Telecommand frames coming from remote (ground) are received by the communications module and sent to the processing module over this bus. For telemetry frames it goes the other way round.

- **Payload Bus**: The payload bus is also a redundant UART and is used to transfer the large amount of data coming from the payload (camera) to the processing module for further processing (such as packing it into the CFDP file protocol to be send to remote via the communications module).

The image below shows these interfaces.

![](assets/rover_interfaces.png)

## System Description

In this section we describe the LibreCube elements to be used for the rover. Some
of these elements do exist already (published [here](https://gitlab.com/librecube/elements)).
Many others are still under development and in prototype stage (see [here](https://gitlab.com/librecube/prototypes)). It is foreseen that the prototypes mature into final elements during
the course of this rover project.

### Structure

For the structure we use the [2U Structure LC2102](https://gitlab.com/librecube/elements/LC2102).
It can integrate a stack of eleven boards.

### Power Module

The supply of electrical power will be achieved by the [Power Module LC2201](https://gitlab.com/librecube/elements/LC2201). It provides a permanent 5V output and eight switchable
output lines. It has two Li-Ion batteries integrated on the board. The batteries
can be charged via USB-C connector or through the CHARGE line on the 104-pin connector.

### Communications Module

The [Communications Module](https://gitlab.com/librecube/prototypes/platform-coms-wifi)
is under development. It uses Wifi for the radio communication and sends the data
via UDP protocol.

The module is constantly listening on the radio channel for incoming data. Data
is received as a sequence of bytes. This byte sequence is scanned to find
the sync marker (a defined byte sequence), which marks the begin of a CCSDS
frame. The frame is then extracted and put on the Comms Bus to be received
and further processed by the Processing Module. Data flowing in this direction
(the forward channel) typically are commands.

Data flowing in the other direction (the return channel) is typically telemetry.
It starts at the Processing Module where the data is sent as CCSDS frames over
the Comms Bus to the Communications Module. There is is sent over radio.

In the future, this module will be replaced with one that uses UHF and/or S-Band
instead of Wifi.

### Processing Module

The [Processing Module](https://gitlab.com/librecube/prototypes/platform-cdhu-pyboard)
is also under development. It is powered by a [pyboard](https://docs.micropython.org/en/latest/pyboard/quickref.html).

The Processing Module uses the System Bus for control and monitoring of the
other modules. It acts as controller node, whereas the other modules act as
responder nodes (if they are connected to System Bus).

The software running on the Processing Module has to support the following:

- [SpaceCAN protocol](https://gitlab.com/librecube/lib/micropython-spacecan) for System Bus communication
- CCSDS Frames
- [CCSDS Space Packets](https://gitlab.com/librecube/lib/python-spacepacket)
- [CFDP protocol](https://gitlab.com/librecube/prototypes/micropython-cfdp) for file exchange
- ECSS PUS Service
- Onboard Control Procedures (OBCP) for Automation

For a more powerful alternative, a [RaspberryPi-powered](https://gitlab.com/librecube/prototypes/platform-cdhu-raspi) version is under development as well.

### Wheel Drive Module

The [Wheels Module LC2801](https://gitlab.com/librecube/elements/LC2801) provides
the four wheels and its driving control.

### Camera Module

_not yet started_

### Solar Panels

The [Solar Panels](https://gitlab.com/librecube/prototypes/platform-solar-panels) are
under development. They will provide charging power to the Power Module.

### Battery Expansion Module

The [Battery Expension Module](https://gitlab.com/librecube/elements/LC2202) simply
extends the capacity of the Power Module by adding three Li-Ion batteries in
parallel via the VBAT connection on the 104-pin connector.

### Navigation Module

The [Navigation Module](https://gitlab.com/librecube/prototypes/platform-guidance-board) is under development. It will provide as a minimum the position information from GNSS.

## Current Status

### Flat Model

The flat model uses the [Flatsat LC3101](https://gitlab.com/librecube/elements/LC3101)
to allow convinient working on the modules.

![](assets/rover_flat.jpg)

### Assembled

The assembled model uses a number of empty boards to fill up the space in
the 2U structure.

![](assets/rover_assembled.jpg)
