# SpaceCAN

The [CAN bus](https://de.wikipedia.org/wiki/Controller_Area_Network) is a robust vehicle bus standard designed to allow microcontrollers and devices to communicate with each other. It is a message-based protocol, designed originally for multiplex electrical wiring within automobiles and is also heavily used in robotics and aerospace.

CAN has many years of heritage and is also qualified for space use. In particular, [ECSS](https://ecss.nl) has developed the CANbus Extenstion Protocol Standard [ECSS-E-ST-50-15C](assets/ECSS-E-ST-50-15C.pdf) specifically for spacecraft projects that opt to use the CAN Network for on-board communications and control. It also defines the optional use of the CANopen standard as an application layer protocol operating in conjunction with the CAN Network data link layer.

For LibreCube's purpose however, the ECSS CANbus is deemed too complex in terms of implementation and usage. Thus we have further modified the ECSS CANbus Standard to fit the needs of typical small spacecraft (and rovers, drones, etc.), while being easy to use and implement. This is what we call the **SpaceCAN** bus.

We consider that the SpaceCAN bus is used for control and monitoring, in particular the exchange of commands and telemetry among spacecraft subsystems. Typically, a central processing unit (the onboard computer) is commanding other intelligent nodes (such as the power system, communication system, and payloads) and collects status information from them. The data to be exchanged on this bus is of low to moderate volume but must be transmitted in a reliable way. The bus is not intended to deliver high-volume data, such as science data. Instead, it shall ensure reliable and robust communication between controller and responder nodes of the network. For this, small messages are sent between nodes that must arrive without error and with minimum delay.

## Topology

The system bus is designed as a linear bus and is composed of a nominal and redundant chain (bus A and bus B).

![](assets/topology.png)

The controller node can talk to responders and the responders can talk to the controller. The responders **do not** talk with each other. If data needs to be transferred from one responder to the other, this must be coordinated by and go through the controller. The concept behind this architecture is that of a central computer that manages the satellite and which is connected to intelligent subsystems (i.e. that have their own microcontrollers). The interconnection of the controller and the various responder nodes form a network.

The network is thus composed of a single controller (with node ID 0) and up to 127 responders (with node ID 1 to 127). The node IDs are typically hard-coded in software and do not change during operation. Node IDs with lower value have higher priority in communication. That means, critical systems must be given lower IDs.

## Bus Architecture

Nodes can connect to the bus via selective or parallel access.

The selective bus access architecture implements a single CAN controller and interfaces the redundant CAN network via two transceivers. A bus selection mechanism is implemented between the CAN controller and the transceivers allowing the application to select the bus to be used for communication.

The parallel bus access architecture interfaces the redundant CAN network through a pair of CAN controllers.

| ![](assets/selective_bus_access_architecture.png) | ![](assets/parallel_bus_access_architecture.png) |
| ------------------------------------------------- | ------------------------------------------------ |
| Selective Bus Access                              | Parallel Bus Access                              |

## CAN-ID Format

The CAN data frames carry an 11-bit field for the CAN ID which identify the message and provide a priority scheme (lower CAN IDs have higher priority). ECSS-CAN, which is based on CANopen, splits the CAN ID into a 4-bit function code (to identify the service) and a 7-bit node ID address. The function code together with the node ID then forms a communication object.

| Object           | CAN ID (hex)  | Originator |
| ---------------- | ------------- | ---------- |
| Heartbeat        | 700           | Controller |
| Sync             | 080           | Controller |
| SCET Time        | 180           | Controller |
| UTC Time         | 200           | Controller |
| Telecommand (TC) | 280 + Node ID | Controller |
| Telemetry (TM)   | 300 + Node ID | Responder  |

## Redundancy Management via Heartbeats

The system bus is made resilient to single point failure (such as problem in cabling or connector fault) through redundant physical media topology. Redundant communication channels require a redundancy management scheme. The selected scheme for cold redundancy (that is, one bus active at a time) applies the concept of node monitoring via heartbeat frames.

The controller node defines the bus to be considered active by periodic transmission of heartbeat frames (CAN ID = 0x700, no data) on the active bus. The heartbeat period is typically in the range of several hundred milliseconds. The controller can switch over and operate on the alternate bus by stopping transmission of the heartbeat messages on the active bus and starting them on the alternate bus, which then becomes the active bus.

The responder nodes monitor the presence of the heartbeat from the controller to determine the active bus. It follows this logic:

- When a responder node misses the controller heartbeat for a defined number of times (`max_miss_heartbeat`) it shall switch to the alternate bus and listen there for heartbeats.
- If it detects a heartbeat on that bus, it shall consider this one as the active bus.
- If no heartbeat is received after `max_miss_heartbeat` times, it shall switch again to the other bus.
- This bus toggling shall be continued for a predefined number of times (`max_bus_switch`) or for infinite time (if not set).

This way, the responder nodes will try to find the controller heartbeats and when found, stay on the active bus.

The decision on when the controller node initiates a bus switch is application specific and not prescribed here. Typically, when responder nodes do not respond to controller commands the controller node may try a bus switch-over to detect if a bus communication problems exists.

## Synchronization

Synchronous network behavior is achieved with the SYNC protocol. The controller node periodically transmits SYNC frames (CAN ID = 0x080, no data) to indicate to the responders to start their application-specific behavior. This could trigger for example the initiation of measurements or the sending of telemetry to the controller node. The SYNC period is usually in the range of a few seconds.

## Time Distribution

Typically, the central on-board computer manages the spacecraft time. In addition, other systems on the spacecraft may also maintain a local time (for example an attitude control system with its own processing unit). The time distribution protocol specified here distributes a sample of the central time to devices maintaining a local time. When and how often the central time is distributed to time consumers is application specific.

**SCET Format**

The controller node shall maintain time information using spacecraft elapsed time (SCET) as defined in clause 3.2 of [CCSDS 301.0-B-4](assets/301x0b4e1.pdf). The time code format of the SCET is the CCSDS unsegmented time code (CUC): an binary count of seconds and binary power of sub-seconds. The SCET is thus a free running counter of up to 2^32 seconds (coarse time) and sub-second representations (fine time) down to 2^-8, 2^-16 or 2^-24.

The SCET time frame has CAN ID = 0x180 and the following data payload:

**UTC Format**

If the spacecraft provides the optional service of maintaing the UTC on board, the format of the UTC shall be that of the CCSDS day segmented time code(CDS): a 16 bit representation of the number of days elapsed from a predefined epoch (e.g. 1 Jan 2000), 32 bits representing the number of ms of the day and an optional 16 bit field of submilliseconds.

The UTC time frame has CAN ID = 0x280 and the following data payload:

| ![](assets/scet_data_format.png) | ![](assets/utc_data_format.png) |
| -------------------------------- | ------------------------------- |
| SCET Data Format                 | UTC Data Format                 |

## Telecommands and Telemetry

Telecommands are CAN frames that are sent from controller to responder, whereas telemetry are frames sent from responders to the controller. Each frame contains data of up to 8 bytes. How the data is interpreted is application specific.

**Telecommands**

The controller node can send telecommands to responder nodes. Telecommands usually trigger some kind of action, like switching a unit on or off, changing the mode or configuration of a unit, deploying a solar panel, etc. A telecommand frame contains the node ID of the node to which the telecommand is being sent to (in the CAN header) and the datafield of up to 8 bytes.

**Telemetry**

The responder nodes send telemetry to the controller node. The sending of telemetry frames from responder nodes is triggered by the controller node either via: a) a specific telecommand that serves as telemetry request, or b) the periodic SYNC frame. Telemetry comprises of essential information about the nodes (also called housekeeping data), such as operational status and sensor readings (temperatures, currents, voltages, etc.). A telemetry frame contains the node ID of the responder node (as indication to the controller from which node the data originates) and the datafield of up to 8 bytes.

## Packet Protocol

To circumvent the limitation of 8 bytes of data to be exchanged, the packet protocol was added. The concept is simple: a packet (that is, a large piece of data) is fragmented into smaller parts of up to 6 bytes that fit into a frame. The first two bytes of a frame data field are used for reassembling them back into the packet. The first byte tells the total number of frames that make up the packet and the second byte is the counter of each frame.

Important: The packet protocol utilizes the telecommand and telemetry frames and thus they cannot be used at the same time. So either use the packet protocol, or the telecommand/telemetry protocol defined above.

Each frame of the packet protocol looks like this:

- Byte 1: total number of frames for this packet minus 1
- Byte 2: current number of this frame, starting from 0
- Byte 3-8: data chunk

For example, to send a packet with 4 bytes size, it fits in a single frame and looks like this:

- frame 1: [0, 0, <4 bytes of data>]

Another example, to send a packet with 8 bytes size, it fits in two frames and looks like this:

- frame 1: [1, 0, <6 bytes of data>]
- frame 2: [1, 1, <2 bytes of data>]

## SpaceCAN Service Protocol

The packet protocol allows for sending larger amount of data, but it still leaves the interpretation of this data completely to the user. In order to allow for a more standardized communication, and thus rapid integration of new modules, the SpaceCAN protocol supports the SpaceCAN Service Protocol (SSP). This protocol is very much inspired from the ECSS Packet Utilization Protocol [ECSS-E-ST-70-41C](assets/ECSS-E-ST-70-41C.pdf) and tries to be as close as possible to its syntax.

The implementation again is fairly simple: each packet (which can have a maximum length of 256 \* 6 bytes = 1536 bytes) carries as first byte a service and as second byte a subtype identifier.

Currently only a subset of the many ECSS PUS services are implemented, namely those services that are mostly required. Other services may be added over time, when need arises.

### ST[01] Request Verification Service

This service reports to the controller the status of the acceptance and execution of a request sent to a responder. An acceptance report is generated immideatly after completion of validity checks of the received request; an execution report is generated after the execution of the request.

#### Controller to Responder

_n/a_

#### Responder to Controller

[1, 1] success acceptance report

- data: the source service and subtype

[1, 2] fail acceptance report

- data: the source service and subtype

[1, 7] success completion execution report

- data: the source service and subtype

[1, 8] fail completion execution report

- data: the source service and subtype

### ST[03] Housekeeping Service

The housekeeping service type provides the visibility of any on-board parameters assembled in housekeeping reports.
The structure of the reports used by the housekeeping service are predefined.

#### Controller to Responder

[3, 5] enable periodic housekeeping reports

- data: the ids of the reports to be enabled

[3, 6] disable periodic housekeeping reports

- data: the ids of the reports to be disabled

[3, 27] request single shot housekeeping reports

- data: the ids of the reports to be requested

#### Responder to Controller

[3, 25] housekeeping report

- data: report id and encoded data

### ST[08] Function Management Service

#### Controller to Responder

[8, 1] perform function

- data: the function id and function arguments, if any

#### Responder to Controller

_n/a_

### ST[17] Test Service

The test service type provides the capability to activate test functions implemented on-board and to report the results of such tests.

#### Controller to Responder

[17, 1] perform a connection test

- data: none

[17, 3] perform an application connection test

- data: application process id (apid)

#### Responder to Controller

[17, 2] connection test report

- data: none

[17, 4] application connection test report

- data: application process id (apid)

### ST[20] Parameter Management Service

The parameter management service type provides capabilities to the controller for reading current values of on-board parameters from responders.

#### Controller to Responder

[20, 1] request report of parameter values

- data: the ids of requested parameters

#### Responder to Controller

[20, 2] parameter value report

- data: total number of parameters in the report, followed by couples of parameter id and encoded parameter value

---

**Reference Implementations**:

- Python: [https://gitlab.com/librecube/prototypes/python-spacecan](https://gitlab.com/librecube/prototypes/python-spacecan)
- MicroPython: [https://gitlab.com/librecube/lib/micropython-spacecan](https://gitlab.com/librecube/lib/micropython-spacecan)
- mBed C++: [https://gitlab.com/librecube/lib/mbed-spacecan](https://gitlab.com/librecube/lib/mbed-spacecan) (outdated!)
