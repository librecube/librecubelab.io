# Standards

In general, there are two main categories of standards that are relevant for us: 

- Technology: Standards in this category define interfaces (for example electrical interfaces), communication protocols, and other aspects related to technology development.
- Processes: Standards in this category prescribe how to run projects, in terms of project management, system engineering, and product assurance.

We only make use of standards that are freely available to the public, and
we have a strong preference for standards from the space domain. There are
two big organizations that do exactly that:

- [European Cooperation for Space Standardization (ECSS)](https://ecss.nl/)
- [Consultative Committee for Space Data Systems (CCSDS)](https://public.ccsds.org)

Reading through standards is a challenging task. Therefore, you may find this
handbook useful that gives an overview on most of those standards:

- [Handbook of Free and Open Space Standards](assets/Handbook_of_Free_and_Open_Space_Standards.pdf)


## LibreCube Standards

For some applications or use cases, there may not be a suitable standard available.
This is for example the case for the CubeSat boards, i.e. those printed circuit boards (PCBs) that resemble the PC/104 form factor and are found in most CubeSats.

In such case, we develop a standard ourself. These standards are then applicable
to all LibreCube projects and may also be used freely for any other projects.

So far, the following standards have been developed by LibreCube:

- [LibreCube Board Specification](board_specification/index.md): Defines the mechanical, electrical, and software protocol interface for the unit boards.
- [SpaceCAN](spacecan/index.md): Defines the reliable onboard CAN protocol for the system communication bus.
