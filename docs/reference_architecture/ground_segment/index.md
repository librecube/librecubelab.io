# Ground Segment

The ground segment comprises those elements that are used to control and monitor
the spacecraft and to process the data returned from it.

## Ground Station System

To be written...

### Antenna System

To be written...

### Station System

To be written...

### Network System

To be written...

## Mission Operations System

The mission operations system consists of a number of elements that on the
on side interface with the remote segment for monitoring and control, and on
the other side have (mission-specific) interface to the user segment that drives
the mission plan and receives the collected mission data.

![](assets/mission_operations_architecture_overview.png)

### Mission Control System

The purpose of the mission control system (MCS) is to give operators
the means to monitor the spacecraft state and to send commands to it.

For a good overview of the functionalities required from an MCS have a look
at [Yamcs](https://docs.yamcs.org/yamcs-server-manual/general/).

### Mission Planning System

To be written...

### Automation System

- [https://gitlab.com/librecube/prototypes/python-pluto](https://gitlab.com/librecube/prototypes/python-pluto)
- [https://gitlab.com/librecube/prototypes/pluto-editor](https://gitlab.com/librecube/prototypes/pluto-editor)

When we talk about automation, we refer to carrying out  activities through the
mission control system in an automated manner. Those activities can be triggered
by human, or through a prepared schedule. Activities mostly comprise of
the sending of telecommands and the monitoring of telemetry parameters.
Such activities are most often captured in procedures.

Procedures are written for nominal operations and also for contingency operations.
Whereas manual procedures are written as text, procedures to be used by automation
systems are typically scripts written in a domain specific language (DSL).
One example of a procedure DSL is the ECSS PLUTO language, which is still readable
by humans and parsable by machines.

The automation system interfaces with the mission control system and mimics the
behaviour of a human operator, that is, loading and sending of telecommands and
reacting to telemetry parameters.

The protocol for this interface is **to be defined**.

### Mission Support

To be written...

### Data Archive and Dissemination

To be written...

### Simulator

- [https://gitlab.com/librecube/prototypes/python-libresim](https://gitlab.com/librecube/prototypes/python-libresim)

The main purpose of a simulator is to replicate the actions and reactions of the
real spacecraft during operations. The simulator is used during mission preparation
for training, procedure validations and ground segment testing, and during mission
operations to test proposed changes to the system before they are deployed to the
real spacecraft.

The simulator connects to the mission control system via the telecommand and
telemetry data links using the SLE protocol. For the MCS it appears as it would
communicate with the real spacecraft, in real time. Oftentimes, an emulator is
part of the simulator that emulates the spacecraft hardware, and runs the real
flight software.

![](assets/simulator_architecture_overview.png)
