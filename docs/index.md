# Welcome to the LibreCube Documentation!

At [LibreCube](https://librecube.org) we want to make space accessible to everyone!
We believe that space exploration is relevant to all humankind and everyone with
interest in space should be given the chance to learn and contribute to it.

We start small. That is, small in size, but not in vision. Our systems are
inspired from the modular [CubeSat standard](https://www.cubesat.org/), which
sparked a revolution in making space more accessible.

We are creating an ecosystem of modular components (such as
electrical power systems, navigation modules, onboard computers) that can
be assembled together to form systems for space and terrestrial exploration,
such as satellites, drones, rovers, and more!

Everything we do is based on these three principles:

- [Open Source](open_source.md): Not only do we publish everything
  as open source, we also strictly only use open source tools to get our
  projects done – so that you can modify everything to your needs, at no costs.

- [Free and Open Standards](standards/overview.md): We rely on proven and tested standards for our system
  designs, mostly using standards from the space domain.

- [Reference Architecture](reference_architecture/overview.md): We create an ecosystem of reusable elements, based on
  a generic architecture of systems and modules that have standardized interfaces.
  This makes it possible to combine and tailor elements for various mission applications.

![](top_level_segments.png)

## What's in for you?

Do you want to be part of a community that develops open source hardware and
software for exploration of space? Do you want to democratize science and
technology? Or maybe just want to tinker on interesting projects in
your free time?

We look for people that are interested in electronics, mechanics, programming,
documentation, outreach, art, ..., everything!

As a contributor to LibreCube, you will learn the power (and frustration) of
open source projects. Moreover, you will learn
quite a lot about space technology and space standards -
the same stuff that is used for building the big missions!

Of course, you are also free to just _use_ the elements ecosystem to build something
on your own (for example, a spacetech-powered pumpkin farming machine) without actively
contributing back. But if you do so, please at least let us know and share your story.

## How to get involved?

No matter if you are absolute beginner or seasoned expert, everyone is welcome.
We want to foster an encouraging environment where people can openly share
ideas, advice, and lessons learned. Please read our [Code of Conduct](code_of_conduct.md).

The best way to start is to go to the [Community Page](https://librecube.org/community/)
to learn how to get engaged with the community.

Have a look at the [Development Section](development/overview.md) to understand how we do software and hardware development. We are also working on larger scale projects that
you may want to join. You can find them [here](projects/overview.md).

## How is the community organized?

Generally, people contribute to LibreCube voluntarily without monetary compensation.
From time to time there are opportunities, such as
[Google Summer of Code](https://summerofcode.withgoogle.com/), that
allows us to have contributors (mostly students) get a stipend for a few months
while working on specific projects.

In 2024 we formally registered LibreCube as a non-profit organization in Germany,
to be able to receive funding and membership fees (due to archaic law however, only EU
citizens can apply for membership and can then pay those yearly fees). But don't worry,
you do not have to be a formal member of this association to be a contributor!

You can always reach the entire community in the [Chat](https://app.element.io/#/room/#librecube.org:matrix.org)!

---

_If you like to contribute to this documentation, go to the [source code](https://gitlab.com/librecube/librecube.gitlab.io)_.

_All documentation is licensed as [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)_
