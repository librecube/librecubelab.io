# Open Source

At LibreCube we are convinced that the philosophy of open source is urgently
needed to democratize space exploration! We aim to use and produce software that is cross-platform and runs on most common operations systems, but the preference is Linux.

> A good starting point to find open source replacements to proprietary software is to go to [https://alternativeto.net/](https://alternativeto.net/) and filter for open source alternatives.

## Open Source Software Tools

Listed here are open source tools we commonly use and recommend as baseline.

**Office and Collaboration**

- [LibreOffice](https://www.libreoffice.org/): The LibreOffice suite comprises programs for word processing, the creation and editing of spreadsheets, slideshows, diagrams and drawings, working with databases, and composing mathematical formulae.
- [DBeaver](https://dbeaver.jkiss.org/): DBeaver is an SQL client and a database administration tool. It provides an editor that supports code completion and syntax highlighting. This is a desktop application written in Java and based on Eclipse platform.
- [GitLab](https://gitlab.com/): GitLab is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more. It can be self-hosted on your own servers, in a container, or on a cloud provider.
- [OpenProject](https://www.openproject.org): OpenProject is a free and open source software for classic, agile or hybrid project management. It offers features like Gantt charts, agile boards, team planner, time and cost reporting, and secure hosting options.
- [Taiga](https://www.taiga.io/): Taiga is a agile project management web application for software developments and other projects. It provides Kanban or Scrum methodology. Backlogs are shown as a running list of all features and User Stories added to the project.

**Engineering**

- [FreeCAD](http://www.freecadweb.org/): FreeCAD is a general-purpose parametric 3D CAD modeler and a building information modeling software with finite-element-method support. FreeCAD is aimed directly at mechanical engineering product design. FreeCAD can be used interactively, or its functionality can be accessed and extended using the Python programming language.
- [KiCad](http://www.kicad-pcb.org/): KiCad is a software suite for electronic design automation (EDA). It facilitates the design of schematics for electronic circuits and their conversion to PCB designs. It features an integrated environment for schematic capture and PCB layout design. Tools exist within the package to create a bill of materials, artwork, Gerber files, and 3D views of the PCB and its components.
- [OpenSCAD](http://www.openscad.org/): OpenSCAD is a free software application for creating solid 3D CAD objects. It is a script-only based modeller that uses its own description language; parts can be previewed, but it cannot be interactively selected or modified by mouse in the 3D view. An OpenSCAD script specifies geometric primitives (such as spheres, boxes, cylinders, etc.) and defines how they are modified and combined (for instance by intersection, difference, envelope combination and Minkowski sums) to render a 3D model.
- [Blender](http://www.blender.org/): Blender is a professional 3D computer graphics software toolset used for creating animated films, visual effects, art, 3D printed models, interactive 3D applications and video games.
- [Papyrus](https://eclipse.org/papyrus/): Papyrus is an UML tool based on Eclipse. It can either be used as a standalone tool or as an Eclipse plugin. It provides support for Domain Specific Languages and SysML. Papyrus is designed to be easily extensible as it is based on the principle of UML Profiles.

**Software Development**

- [Code OSS](https://github.com/code-oss-dev/code/): Code OSS is the open source core of Visual Studio Code without the Microsoft-specific customizations.
- [VSCodium](https://vscodium.com/): VSCodium is a community-driven, freely-licensed binary distribution of Microsoft's editor VS Code.
- [PlatformIO](https://platformio.org/): PlatformIO is a cross-platform IDE solution for embedded software development.

## Open Source Licenses

To publish your works as open source, you must include an open source license
in your project repository to let others know under what exact conditions they
can make use of your works.

You are free to decide which license to use (but stick to [OSI approved licenses](https://opensource.org/licenses)!)

Here is a guideline to help choosing an appropriate license:

- **The MIT License**: We use this one for software libraries, to allow others
  to modify them to their needs. It is a very permissive license.

- **GNU General Public License version 3**: We use this mostly for software
  applications. This way, anyone can run/modify/distribute the software, but they
  are not allowed to integrate it in closed-source projects. Derived works must
  remain open source.

- **CERN Open Hardware Licence Version 2**: We use this one for hardware projects.
